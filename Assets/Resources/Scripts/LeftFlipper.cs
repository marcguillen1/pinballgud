﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftFlipper : MonoBehaviour {

    Rigidbody fliprbody;
    public Vector3 startPos;
    public string side;
	// Use this for initialization
	void Start () {
        fliprbody = transform.GetComponent<Rigidbody>();
        startPos = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Z))
        {
           fliprbody.AddForce(new Vector3(0, 0, +100f), ForceMode.Impulse);
        }
        else fliprbody.AddForce(new Vector3(0, 0, -100f), ForceMode.Impulse);
	}
}
