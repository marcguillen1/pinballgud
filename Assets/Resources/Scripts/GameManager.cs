﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GameManager : MonoBehaviour {

    public static bool gameOverG;
    public static int lives;
    public static int score;
    public static int highscore;
    public static int multi;
    public static int targets;
    public static int lightactiveTime;
    public static bool ballingame;
    public static GameObject[] targets_ar;
    private static bool canRestartTargets;
    private static Text scoreText;
    private static Text liveText;
    private static Text multiText;
    private static Text targetText;
    private static Text mainText;
    private static Image logo;
    private static RectTransform scoreTr;
    private static RectTransform liveTr;
    private static RectTransform multiTr;
    private static RectTransform targetTr;
    private static RectTransform logoTr;
    private static RectTransform mainTr;
    public static GameObject[] lights;
    public  GameObject ball;
    // Use this for initialization

    void Awake()
    {
        scoreText = GameObject.Find("Score").GetComponent<Text>();
        scoreTr = GameObject.Find("Score").GetComponent<RectTransform>();      
        liveText = GameObject.Find("Life").GetComponent<Text>();
        liveTr = GameObject.Find("Life").GetComponent<RectTransform>();
        multiText = GameObject.Find("Multiplier").GetComponent<Text>();
        multiTr = GameObject.Find("Multiplier").GetComponent<RectTransform>();
        targetText = GameObject.Find("Targets").GetComponent<Text>();
        targetTr = GameObject.Find("Targets").GetComponent<RectTransform>();
        logo = GameObject.Find("Logo").GetComponent<Image>();
        logoTr = GameObject.Find("Logo").GetComponent<RectTransform>();
        mainText = GameObject.Find("MainText").GetComponent<Text>();
        mainTr = GameObject.Find("MainText").GetComponent<RectTransform>();
    }
    void Start () {
        gameOverG = false;
        lives = 3;
        score = 0;
        multi = 1;
        targets = 4;
        ballingame = false;
        targets_ar = GameObject.FindGameObjectsWithTag("Target");
        lights = GameObject.FindGameObjectsWithTag("Lights");
        canRestartTargets = false;
        logoTr.DOScale(new Vector3(1,1,1), 1f);
        logo.DOFade(120, 1f);
        mainTr.DOScale(new Vector3(1, 1, 1), 1f);
    }
	
	// Update is called once per frame
	void Update () {
        //updating all variables
        scoreText.text = "score: " + score.ToString();
        targetText.text = "targets: " + targets.ToString();
        liveText.text = "lives: " + lives.ToString();
        multiText.text = "x" + multi.ToString();

        if (targets == 0)
        {
            multi = 3;
            Invoke("prepareTargets", 1.5f);
            lightactiveTime = 5;
            lighting();
        }

        if(lives == 0)
        {
            gameOverG = true;
            Invoke("gameOver", 1f);
        }
       
	}

    void prepareTargets()
    {
        foreach (GameObject target in targets_ar)
        {
            if (target.GetComponent<Target>().down)
            {
                target.GetComponent<Target>().resetTargets();
            }
        }
        targets = 4;
    }

    public void readyGo()
    {
        mainTr.DOScale(new Vector3(0, 0, 0),0f);
        mainText.alignment = TextAnchor.MiddleCenter;
        mainText.text = "go!";
        mainTr.DOScale(new Vector3(1, 1, 1), 1f);
        Invoke("setGUI", 2f);
        ball.SetActive(true);
        gameOverG = false;
    }

    public void setGUI()
    {
        mainText.text = "";
        mainTr.DOScale(new Vector3(0, 0, 0), 0.5f);
        scoreTr.DOScale(new Vector3(1, 1, 1), 0.5f);
        liveTr.DOScale(new Vector3(1, 1, 1), 0.5f);
        multiTr.DOScale(new Vector3(1, 1, 1), 0.5f);
        targetTr.DOScale(new Vector3(1, 1, 1), 0.5f);
    }

    void gameOver()
    {
        mainText.text = "GAME OVER";
        mainTr.DOScale(new Vector3(1, 1, 1), 1f);
        scoreTr.DOScale(new Vector3(0, 0, 0), 0f);
        liveTr.DOScale(new Vector3(0, 0, 0), 0f);
        multiTr.DOScale(new Vector3(0, 0, 0), 0f);
        targetTr.DOScale(new Vector3(0, 0, 0), 0f);
        if(score > highscore)
        {
            highscore = score;
        }
        Invoke("newHighscore", 1.5f);
        lives = 3;
    }

    void newHighscore()
    {
        mainText.text = "";
        mainText.text = "Highscore! "+ highscore.ToString();
    }

    void lighting()
    {
        foreach (GameObject light in lights)
        {
            light.GetComponent<Lightin>().lightgo();
        }
        mainText.text = "Nice!";
        Invoke("setGUI", 2f);
    }
}
