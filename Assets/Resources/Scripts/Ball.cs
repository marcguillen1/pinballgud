﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    private Vector3 startPos;
    public GameObject barrier;
    private Rigidbody myrbody;
    // Use this for initialization
	void Start () {
        startPos = transform.position;
        myrbody = this.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        if(GameManager.ballingame == true)
        {
            barrier.SetActive(true);
        }
        else
        {
            barrier.SetActive(false);
        }
	}

    void OnTriggerEnter(Collider col)
    {if (!GameManager.gameOverG)
        {
            if (col.name == "RespawnCol")
            {
                Invoke("setBall", 1.5f);
            }
        }
        if (col.name == "Barrier")
        {
            GameManager.ballingame = true;
        }
    }

    void setBall()
    {
        transform.position = startPos;
        GameManager.ballingame = false;
        myrbody.velocity = Vector3.zero;
        GameManager.lives--;
        GameManager.multi = 1;
    }
}
