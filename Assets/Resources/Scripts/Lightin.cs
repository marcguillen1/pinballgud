﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lightin : MonoBehaviour {

    private Light mylight;

    void Start()
    {
        mylight = this.GetComponent<Light>();
    }

    public void lightgo()
    {
        mylight.intensity = 8;
        mylight.range = 50;
        Invoke("lightsOff", 1f);
        GameManager.lightactiveTime--;
    }

    void lightsOff()
    {
        if(this.name == "Point Light")
        {
            mylight.intensity = 4;
            mylight.range = 2;
        }
        mylight.intensity = 0;
        mylight.range = 0;

        if (GameManager.lightactiveTime >= 0)
        {
            Invoke("lightgo", 0.5f);
        }
    }
}
