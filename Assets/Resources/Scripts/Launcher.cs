﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launcher : MonoBehaviour {

    public Vector3 oldPosition;
    public Vector3 maxPosition;
    private float chargeForce;
    private float impulseForce;
    private Rigidbody myrbody;
	// Use this for initialization

	void Start () {
        chargeForce = 100f;
        impulseForce = 700f;
        myrbody = this.GetComponent<Rigidbody>();
        oldPosition = transform.localPosition;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Space))
        {
            myrbody.constraints = RigidbodyConstraints.None;
            myrbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX;
            myrbody.AddForce(new Vector3(0, -chargeForce, -chargeForce), ForceMode.Force);
        }else if(transform.localPosition.z >= oldPosition.z)
        {
            myrbody.constraints = RigidbodyConstraints.FreezeAll;
        }

        if(Input.GetKeyUp(KeyCode.Space))
        {
            myrbody.AddForce(new Vector3(0, 200f, impulseForce), ForceMode.Impulse);
        }
    }
}