﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {

    public Collider mycol;
    private Light mylight;
    private float finaltransform;
    public bool down;
    void Start()
    {
        mylight = this.GetComponentInChildren<Light>();
        finaltransform = -1.4f;
        down = false;
    }

    void OnCollisionEnter()
    {
        StartCoroutine("goDown");
        mycol.enabled = false;
        mylight.enabled = false;
        GameManager.targets--;
        down = true;
        GameManager.score += 150 * GameManager.multi;
    }

    IEnumerator goDown()
    {
        for(var i = 1.0f; i > finaltransform; i-=0.1f)
        {
            this.transform.localPosition -= new Vector3(0,0.1f,0);
            yield return new WaitForEndOfFrame();
        }
        
    }

    IEnumerator goUp()
    {
        for (var i = 1.0f; i > finaltransform; i -= 0.1f)
        {
            this.transform.localPosition += new Vector3(0, 0.1f, 0);
            yield return new WaitForEndOfFrame();
        }

    }

    public void resetTargets()
    {
        StartCoroutine("goUp");
        mycol.enabled = true;
        mylight.enabled = true;
        down = false;
    }

}
