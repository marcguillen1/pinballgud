﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumper : MonoBehaviour {

    private float force = 3500.0f;
    public Rigidbody ballrbody;
    private float radius = 1.0f;
    private Light mylight;
	// Use this for initialization
	void Start () {
        mylight = this.GetComponentInChildren<Light>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter()
    {
        foreach(Collider col in Physics.OverlapSphere(transform.position, radius))
        {
            if(col.GetComponent<Rigidbody>())
            {
                col.GetComponent<Rigidbody>().AddExplosionForce(force+ballrbody.velocity.magnitude, transform.position, radius);
            }
        }
        mylight.intensity = 8;
        mylight.range = 4;
        Invoke("LightsOK", 0.15f);
        GameManager.score += 50 * GameManager.multi;
    }

    void LightsOK()
    {
        mylight.intensity = 4;
        mylight.range = 2;
    }

}
